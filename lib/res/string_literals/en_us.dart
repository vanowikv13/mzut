const String MAIN_APP_TITLE = "mZut";
const LOGIN_NAME = "Login";
const PASSWORD_NAME = "Password";
const EMPTY_LOGIN_ERROR_MESSAGE = "Login can't be empty";
const EMPTY_PASSWORD_ERROR_MESSAGE = "Password can't be empty";
const LOGIN_BUTTON_TEXT = "Login";
