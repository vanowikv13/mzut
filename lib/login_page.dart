import 'package:flutter/material.dart';
import 'package:mzut/res/string_literals/en_us.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginStatePage();
}

class _LoginStatePage extends State<LoginPage> {
  String _login;
  String _password;

  final formKey = GlobalKey<FormState>();

  bool validateAndSave() {
    final form = formKey.currentState;
    //form is validate at this moment
    if (form.validate()) {
      form.save();
      return true;
    } else
      return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      var client = http.Client();
      try {
        Map<String, String> headers = {"Content-type": "application/json"};
        var url = "https://www.zut.edu.pl/app-json-proxy/index.php?f=getAuthorization";
        var response = await client.post(url, headers: headers, body: '{"login": $_login, "password": $_password, "toekn": "22222222222222222222222222222222"}');
        print(response.statusCode);
        print(response.body);
      } catch (e) {
        print('Error: $e');
      } finally {
        client.close();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(MAIN_APP_TITLE),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 14.0, right: 14.0, top: 8.0),
        child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: buildInputs() + buildSubmitButtons(),
            )),
      ),
    );
  }

  List<Widget> buildInputs() {
    return [
      TextFormField(
        decoration: InputDecoration(labelText: LOGIN_NAME),
        validator: (value) => value.isEmpty ? EMPTY_LOGIN_ERROR_MESSAGE : null,
        onSaved: (value) => _login = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: PASSWORD_NAME),
        validator: (value) => value.isEmpty ? EMPTY_PASSWORD_ERROR_MESSAGE : null,
        obscureText: true,
        onSaved: (value) => _password = value,
      ),
    ];
  }

  List<Widget> buildSubmitButtons() {
      return [
        Container(
          margin: EdgeInsets.only(top: 12.0),
          child: RaisedButton(
            child: Text(
              LOGIN_BUTTON_TEXT,
              style: TextStyle(fontSize: 20.0),
            ),
            color: Colors.green,
            onPressed: validateAndSubmit,
          ),
        ),
      ];
  }
}
